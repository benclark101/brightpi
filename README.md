# BrightPI

A python service that sets the brightness of the official Raspberry PI 7" touchscreen, without the need for any light sensors

## Requirements

This routine requries the use of the Markov Chain Monte Carlo code of https://doi.org/10.1111/j.1365-2966.2007.12195.x

Python 3.7+
You'll also need the following libraries:
* [suntime ](https://pypi.org/project/suntime/)
* [rpi_backlight](https://pypi.org/project/rpi-backlight/)

## Usage

Download the python file and save it in the root directory as the root user. You can then set this by creating the following file:

```/etc/systemd/system/brightpi.service```

Then you can add the following contents:

```
[Unit]
Description=Python Brigtness Timer
After=network.target

[Service]
ExecStart=/usr/bin/python3 /root/brightPI.py
WorkingDirectory=/root
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=pybri
User=root
Group=root
Restart=always

[Install]
WantedBy=multi-user.target
```

