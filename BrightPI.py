import time as timeorig
from datetime import datetime, time, timedelta
from suntime import Sun, SunTimeException
from rpi_backlight import Backlight

#Set appropriate screen Values
lowest_brightness = 10
max_brightness = 100

#Debugging in case of issues
debug=True

#Approximate location for time zone info
latitude = 52.0
longitude = -3.0

#Get solar positions for given location
sun = Sun(latitude, longitude)

check_day=True
while check_day == True:
        #Get sun-rise and sun-set times
        today_sr = sun.get_sunrise_time()
        today_ss = sun.get_sunset_time()
        if debug==True: print('Today the sun raised at {} and get down at {} UTC'.format(today_sr.strftime('%H:%M'), today_ss.strftime('%H:%M')))

        #load Backlight module
        backlight = Backlight()

        today = True
        midnight_yesterday = datetime.combine(datetime.today(), time.min)
        max_time = midnight_yesterday+ timedelta(hours=24,minutes=00)
        print(max_time)
        while today==True:
                this_time = datetime.now()
                if debug == True: print ("Current Time:",this_time)
                if this_time > max_time:
                        break
                #Before Sunrise
                today_sr = today_sr.replace(tzinfo=None)
                today_ss = today_ss.replace(tzinfo=None)
                print("sunrise",today_sr)
                if this_time <= today_sr:
                        if debug == True: print ("Before Sunrise")
                        tdelta = today_sr - this_time
                        tdelta_mins = tdelta.seconds/60.
                        if debug == True:print("Tdelt:",tdelta)
                        if tdelta_mins > 60:
                                if debug == True:print("More than 1hr:",tdelta)
                                backlight.brightness = lowest_brightness
                                if debug == True:print("this_brigtness:",lowest_brightness)
                        else:
                                perc_mins = ((60-tdelta_mins)/60.) * 100
                                perc_bright = (max_brightness-lowest_brightness)/100
                                this_brigtness = lowest_brightness+(perc_bright*perc_mins)
                                if debug == True:print("perc_mins:",perc_mins,"perc_bright",perc_bright)
                                if debug == True:print("this_brigtness:",this_brigtness)
                                backlight.brightness = this_brigtness
                #After Sunset
                elif this_time >= today_ss:
                        if debug == True: print ("After Sunset")
                        tdelta = this_time-today_ss
                        tdelta_mins = tdelta.seconds/60.
                        if debug == True:print("Tdelt:",tdelta)
                        if tdelta_mins > 60:
                                if debug == True:print("More than 1hr:",tdelta)
                                backlight.brightness = lowest_brightness
                                if debug == True:print("this_brigtness:",lowest_brightness)
                        else:
                                perc_mins = ((60-tdelta_mins)/60.) * 100
                                perc_bright = (max_brightness-lowest_brightness)/100
                                this_brigtness = lowest_brightness+(perc_bright*perc_mins)
                                if debug == True:print("perc_mins:",perc_mins,"perc_bright",perc_bright)
                                if debug == True:print("this_brigtness:",this_brigtness)
                                backlight.brightness = this_brigtness
                else:
                        if debug == True: print ("Daytime!")
                        backlight.brightness = max_brightness

                if debug == True:print (this_time)
                timeorig.sleep(5*60)